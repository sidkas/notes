---
marp: true
title: Marp CLI example
description: Hosting Marp slide deck on the web
theme: uncover
class: invert
# paginate: true
# _paginate: false


---
# Stock/Currency Trader Bot Ideas


---
## Software architecture
- Docker & Docker-compose for python
- Custom OpenAI gym environment for training the bot on data
- Trained models stored and tracked
- DL framework - Tensorflow/pytorch/keras/fastai
- bot interation API using Flask/FastAPI

---
## Deploy 
* Scheduling/workflow management - Airflow?
* Cloud services - some container service (AWS, Azure, etc.)
* Visualization - realtime plot of turbulence index & Min-variance vs cummulative return

---
### **Case 1:** 
#### Bot for trading single stock/currency

---
#### **Code Inspiration**
- Kaggle Deep Reinforcement Learning on Stock Data [code](https://www.kaggle.com/itoeiji/deep-reinforcement-learning-on-stock-data)
- Algorithm: dueling deep deterministic q network with prioritized experience replay [DDQN-PER-keras](https://github.com/alexbooth/DDQN-PER) , [DDDQN-tf2](https://github.com/abhisheksuran/Reinforcement_Learning/blob/master/DDDQN.ipynb)
- DRL for automated stocktrading [article](https://towardsdatascience.com/deep-reinforcement-learning-for-automated-stock-trading-f1dad0126a02) [code](https://github.com/AI4Finance-LLC/Deep-Reinforcement-Learning-for-Automated-Stock-Trading-Ensemble-Strategy-ICAIF-2020)




---
### **State space**
* Available balance
* Adjusted close price
* Number of shares owned
* Moving Average Convergence Divergence (MACD)
* Relative Strength Index (RSI) 
* Commodity Channel Index (CCI)
* Average Directional Index (ADX)

---
### **Action space**
* Buy (0,k)
* Hold (0,1)
* Sell (0,k)


---


### **Reward**

- Info
    - *k* : quantitly, *p*: price, *tp*: transactional cost %, 
    - *b*: balance *S*: selling, *B*: buying, *H*: holding
- Transaction cost $c_t = p * k_t * tp$
- Cummutive transition $h_{t+1} = h_t - k^B + k^S$
- Reward, $r(s_t, a_t, s_{t+1})= r_H − r_S + r_B − c_t$  
    - where 
        $r_H = (p_{t+1}^H - p_t^H)*h_t^H$, $r_B = (p_{t+1}^B - p_t^B)*h_t^B$
        $r_S = (p_{t+1}^S - p_t^S)*h_t^S$
---
### Accounting for Turbulence
- turbulence $t = (y_t − µ) . (y_t − µ)^T$

- When turbulence is higher than a threshold, which indicates extreme market conditions, we simply halt buying and the trading agent sells all shares

---
### Testing with the real data 

- Use data/API from [borsdata](https://borsdata.se/)

---
### **Case 2:**
#### Bot for trading multiple stocks/currencies


---
### **Accounting for multiple stocks/currencies**
- DRL for automated mutltiple stocktrading [article](https://towardsdatascience.com/deep-reinforcement-learning-for-automated-stock-trading-f1dad0126a02) [code](https://github.com/AI4Finance-LLC/Deep-Reinforcement-Learning-for-Automated-Stock-Trading-Ensemble-Strategy-ICAIF-2020)

- Discussion: multiple models vs single vectorized model strategy




